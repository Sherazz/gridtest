import { Component } from "@angular/core";
import { ElementRef } from '@angular/core';
import { GridOptions } from "ag-grid/main";
import { CommonModule } from "@angular/common";
import { NgModel } from '@angular/forms';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AgGridModule } from "ag-grid-angular";
import { Observable } from "rxjs/Rx";
import { Injectable } from "@angular/core";
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import { CommentService } from "./api.service";
import {AutoCompleteModule} from 'primeng/autocomplete';
import {AutoComplete} from 'primeng/autocomplete';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { ViewChild } from '@angular/core';
@Component({
    moduleId: module.id,
    selector: 'my-app',
    templateUrl: 'app.component.html'
    // host: {
    //     '(document:click)': 'handleClick($event)',
    // },
})
@Injectable()
export class AppComponent {
    @ViewChild('autocomplete') autocompleteCharge:AutoComplete;
    public gridOptions: GridOptions;
    public rowData: any[];
    public columnDefs: any[];
    public query = '';
    public countries = ["Albania", "Andorra", "Armenia", "Austria", "Azerbaijan", "Belarus",
        "Belgium", "Bosnia & Herzegovina", "Bulgaria", "Croatia", "Cyprus",
        "Czech Republic", "Denmark", "Estonia", "Finland", "France", "Georgia",
        "Germany", "Greece", "Hungary", "Iceland", "Ireland", "Italy", "Kosovo",
        "Latvia", "Liechtenstein", "Lithuania", "Luxembourg", "Macedonia", "Malta",
        "Moldova", "Monaco", "Montenegro", "Netherlands", "Norway", "Poland",
        "Portugal", "Romania", "Russia", "San Marino", "Serbia", "Slovakia", "Slovenia",
        "Spain", "Sweden", "Switzerland", "Turkey", "Ukraine", "United Kingdom", "Vatican City"];
    public filteredList = [];
    public elementRef;
    selectedIdx: number;
    theHtmlString:any;
    a:any;
    b:any;
    c:any;

    constructor(myElement: ElementRef, private http: Http) {
        this.selectedIdx = -1;
        this.elementRef = myElement;
        // we pass an empty gridOptions in, so we can grab the api out
        this.gridOptions = <GridOptions>{
            onGridReady: () => {
                var count = this.gridOptions.api.getDisplayedRowCount();
                console.log("getDisplayedRowCount() => " + count);
                this.gridOptions.api.sizeColumnsToFit();

            }
        };
        this.columnDefs = [
            { headerName: "Make", field: "make", editable: true },
            { headerName: "Model", field: "model", editable: true },
            { headerName: "Price", field: "price", editable: true }
        ];
        this.rowData = [
            { make: "Toyota", model: "Celica", price: 35000 },
            { make: "Ford", model: "Mondeo", price: 32000 },
            { make: "Porsche", model: "Boxter", price: 72000 }
        ];
    }

    selectAllRows() {
        this.gridOptions.api.selectAll();
    }

    newCount = 1;
    createNewRowData() {
        var newData = {
            make: "Toyota " + this.newCount,
            model: "Celica " + this.newCount,
            price: 35000 + (this.newCount * 17)
        };
        this.newCount++;
        return newData;
    }

    printResult(res) {
        console.log('---------------------------------------')
        if (res.add) {
            res.add.forEach(function (rowNode) {
                console.log('Added Row Node', rowNode);
            });
        }
    }
    createcustom(query) {
        var newData = {
            make: query + this.newCount,
            model: query + this.newCount,
            price: 35000 + (this.newCount * 17)
        };
        this.newCount++;
        return newData;
    }
    onAddRow(query) {
        this.autocompleteCharge.inputEL.nativeElement.value = '';
        var newItem = this.createcustom(query);
        var res = this.gridOptions.api.updateRowData({ add: [newItem] });
        this.printResult(res);
        this.getDisplayedRowCount();
    }

    filterCountrySingle(event) {
        let query = event.query;
            this.filteredList = this.filterCountry(query, this.countries);
    }
    filterCountry(query, countries: any[]):any[] {
        //in a real application, make a request to a remote url with the query and return filtered results, for demo we filter at client side
        let filtered : any[] = [];
        for(let i = 0; i < countries.length; i++) {
            let country = countries[i];
            if(country.toLowerCase().indexOf(query.toLowerCase()) == 0) {
                filtered.push(country);
            }
        }
        return filtered;
    }
    // filter(event: any) {
    //     console.log("inside filter" + this.query);
    //     if (this.query !== "") {
    //         this.filteredList = this.countries.filter(function (el) {
    //             return el.toLowerCase().indexOf(this.query.toLowerCase()) > -1;
    //         }.bind(this));
    //         if (event.code == "ArrowDown" && this.selectedIdx < this.filteredList.length) {
    //             this.selectedIdx++;
    //         } else if (event.code == "ArrowUp" && this.selectedIdx > 0) {
    //             this.selectedIdx--;
    //         }
    //     } else {
    //         this.filteredList = [];
    //     }
    // }

    // select(item) {
    //     this.query = item;
    //     this.filteredList = [];
    //     this.selectedIdx = -1;
    //   //  this.onAddRow();
    // }
    getDisplayedRowCount() {
        let row = this.gridOptions.api.getDisplayedRowCount();
        console.log("rowcount " + row);
    }
    createFood() {
        let commentsUrl = 'https://cors-anywhere.herokuapp.com/https://api.jdoodle.com/v1/execute';
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        let body = {
            "clientId": "4abc1ff871005ed8da76d75fb73a6f4c",
            "clientSecret": "2720a0f46911a5f6cc6a863699b6e58164d3f3b0bde99d909c39fef68de4631e",
            "script": "#include<stdio.h>\nint main(){\nint a[3][3],i,j;float determinant=0;\nfor(i=0;i<3;i++)for(j=0;j<3;j++)scanf(\"%d\",&a[i][j]);for(i=0;i<3;i++){for(j=0;j<3;j++)printf(\"%d\t\",a[i][j]);printf(\"\\n\");}printf(\"$\");for(i=0;i<3;i++) determinant=determinant+(a[0][i]*(a[1][(i+1)%3]*a[2][(i+2)%3]-a[1][(i+2)%3]*a[2][(i+1)%3]));for(i=0;i<3;i++){for(j=0;j<3;j++)printf(\"%.2f\t\",((a[(i+1)%3][(j+1)%3]*a[(i+2)%3][(j+2)%3])-(a[(i+1)%3][(j+2)%3]*a[(i+2)%3][(j+1)%3]))/determinant);printf(\"\\n\");}}",
            "language": "c",
            "versionIndex": "0",
            "stdin": "3\n5\n2\n1\n5\n8\n3\n9\n2"
        }
        this.http.post(commentsUrl, body, options).map(res => res.json())
        .subscribe(
          (data) =>{this.theHtmlString= data.output;
            console.log("html"+this.theHtmlString);
             this.a = this.theHtmlString.split('$');
            this.b = this.a[0];
             this.c = this.a[1];
                // var data=data.output;
                // var returnStr = "";
                // var lines = data.split("\n");
                // var block = [];
                // for (var i = 0; i < lines.length; i++) {
                //     block = lines[i].split('\t');
                //     for (var j = 0; j < block.length; j++) {
                //         returnStr += block[j];
                //         if (block.length>1) {
                //             returnStr +=  "\\t";
                //         }
                //     };
                //     returnStr +=  "\\n";
                // };
                // console.log("retrun"+returnStr);
            },
          (err) => console.log(err));
//           var markup = this.theHtmlString;
// var parser = new DOMParser()
// var el = parser.parseFromString(markup, "text/html");
//console.log(el);
    }
}