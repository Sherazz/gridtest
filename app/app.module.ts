// app.module.ts 
import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
// ag-grid
import { NgModel } from '@angular/forms';
import {AgGridModule} from "ag-grid-angular/main";
import {CommonModule} from '@angular/common';
import { FormsModule } from '@angular/forms';
import {HttpModule} from '@angular/http';
// application
import {AppComponent} from "./app.component";
import {CommentService} from "./api.service";
import {AutoCompleteModule} from 'primeng/autocomplete';
import {AccordionModule} from 'primeng/accordion';     //accordion and accordion tab
import {MenuItem} from 'primeng/api';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { ViewChild } from '@angular/core';

@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        CommonModule,
        AgGridModule.withComponents([]
        ),FormsModule,AutoCompleteModule,AccordionModule,BrowserAnimationsModule
    ],
    declarations: [
        AppComponent
    ],
     providers: [
      CommentService
  ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
